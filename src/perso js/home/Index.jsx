import React from 'react';
import { Link } from 'react-router-dom';

function Home() {
    return (
        <div className="Home"> 
            <h1>Bakeli school</h1>
            <p><Link to="users">&gt;&gt; Gerer les utilisateurs</Link></p>
        </div>
    );
}

export { Home };